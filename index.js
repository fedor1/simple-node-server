const { port } = require('./lib/config');
const client = require('./lib/client');

const exitWithError = (err) => {
  console.error(err);
  process.exit(1);
};

process.on('uncaughtException', exitWithError);
process.on('unhandledRejection', exitWithError);

client.start(port, () => {
  process.exit(0);
});
