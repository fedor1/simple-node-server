module.exports = {
  async read(message) {
    return new Promise((resolve, reject) => {
      try {
        if (message) {
          process.stdout.write(message);
        }
        process.stdin.resume();
        process.stdin.setEncoding('utf8');
        process.stdin.once('data', (readMessage) => {
          resolve(readMessage.replace(/\r?\n|\r/g, ""));
        });
      } catch (err) {
        reject(err);
      }
    });
  }
};
