const crypto = require('crypto');

const algorithm = 'aes-256-cbc';

module.exports = {
  generateHash(key) {
    const hash = crypto.createHash('sha256');
    const data = hash.update(key, 'utf8');
    return data.digest();
  },
  encrypt(key, iv, text) {
    console.log('iv', iv);
    const cipher = crypto.createCipheriv(algorithm, key, iv);
    return cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
  },
  decrypt(key, iv, text) {
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    return decipher.update(text, 'hex', 'utf8') + decipher.final('utf8');
  },
};
