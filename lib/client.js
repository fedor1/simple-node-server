const net = require('net');

const consoleReader = require('./consoleReader');
const { COMMANDS } = require('./consts');
const { decrypt, encrypt, generateHash } = require('./cryptoHelper');

const handlers = {
  [COMMANDS.READY]: 'onReady',
  [COMMANDS.RECEIVE]: 'onReceive',
};

class Client {
  start(port, onStop) {
    this.onStop = onStop;
    return new Promise((resolve) => {
      this.connection = net.connect({ port }, () => {
        console.log('connected to server!');
        resolve();
      });

      this.connection.on('data', data => {
        const message = JSON.parse(data.toString());
        const handlerName = handlers[message.command];
        if (!handlerName) {
          console.error(`Unsupported command ${message.command}`);
          return;
        }
        this[handlerName](message.payload);
      });

      this.connection.on('end', () => {
        console.log('disconnected from server');
        if (this.onStop) {
          this.onStop();
        }
      });
    });
  }

  async work() {
    const secret = await consoleReader.read('Enter secret or enter to skip:\n');
    if (secret) {
      this.key = generateHash(secret);
    }
    console.log('Enter your message/(room|-1):');
    while (true) {
      const rawMessage = await consoleReader.read();
      const [message, room] = rawMessage.split('/');
      this.sendCommand(COMMANDS.SEND, {
        message: this.key ? encrypt(this.key, this.iv, message) : message,
        room,
      });
    }
  }

  sendCommand(command, payload) {
    const rawMessage = JSON.stringify({ command, payload });
    console.log(`send: ${rawMessage}`);
    this.connection.write(Buffer.from(rawMessage));
  }

  onReady({ room, iv }) {
    console.log(`You joined to room ${room}`);
    this.iv = iv;
    this.work()
      .catch((err) => {
        console.error('Cannot work anymore');
        console.error(err);
        if (this.onStop) {
          this.onStop();
        }
      });
  }

  onReceive({ fromRoom, message, sentAt }) {
    if (this.key) {
      try {
        message = decrypt(this.key, this.iv, message);
      } catch (err) {
        // just silently skip
      }
    }
    console.log(`
received: ${message}
  from: ${fromRoom}
  at: ${sentAt}`);
  }
}

module.exports = new Client();
