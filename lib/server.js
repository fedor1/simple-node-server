const crypto = require('crypto');
const net = require('net');

const { ALL_ROOM, COMMANDS } = require('./consts');

const handlers = {
  [COMMANDS.SEND]: 'onSend',
};

const sendToConnection = (buffer, connection) => {
  connection.write(buffer);
};

class Server {
  constructor() {
    this.iv = crypto.randomBytes(8).toString('hex');
    this.roomsCounter = 0;
    this.roomMap = {
      [ALL_ROOM]: [],
    };
  }

  start(port) {
    const server = net.createServer(connection => {
      console.log('client connected');
      const room = this.addClient(connection);
      connection.on('data', data => {
        const messageStr = data.toString();
        console.log(`received: ${messageStr}`);
        const message = JSON.parse(messageStr);
        const handlerName = handlers[message.command];
        if (!handlerName) {
          console.error(`Unsupported command ${message.command}`);
          return;
        }
        this[handlerName](room, message.payload);
      });
      connection.on('end', () => {
        this.removeClient(connection);
        console.log('client disconnected');
      });
      this.sendCommand(room, COMMANDS.READY, { room, iv: this.iv })
    });
    server.listen(port, () => {
      console.log(`server is listening on ${port}`);
    });
  }

  addClient(connection) {
    this.roomsCounter += 1;
    const room = `${this.roomsCounter}`;
    this.roomMap[room] = connection;
    this.roomMap[ALL_ROOM].push(connection);
    return room;
  }

  removeClient(connection) {
    const foundEntry = Object.entries(this.roomMap).find(([, value]) => value === connection);
    if (!foundEntry) {
      console.error('Unrecognized connection');
      return;
    }
    delete this.roomMap[foundEntry[0]];
    this.roomMap[ALL_ROOM] = this.roomMap[ALL_ROOM].filter(item => item !== connection);
  }

  sendCommand(room, command, payload) {
    const connection = this.roomMap[room];
    if (!connection) {
      console.error(`Unknown room ${room}`);
      return;
    }
    const rawMessage = JSON.stringify({ command, payload });
    console.log(`sending to ${room}: ${rawMessage}`);
    const buffer = Buffer.from(rawMessage);
    // We expect array here when room contain several connections. I dislike this code, but that makes it shorter
    if (Array.isArray(connection)) {
      connection.forEach(sendToConnection.bind(undefined, buffer));
    } else {
      sendToConnection(buffer, connection);
    }
  }

  onSend(fromRoom, { message, room = ALL_ROOM }) {
    const sentAt = new Date().toISOString();
    this.sendCommand(room, COMMANDS.RECEIVE, { fromRoom, message, sentAt });
  }
}

module.exports = new Server();
