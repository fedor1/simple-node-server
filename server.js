const { port } = require('./lib/config');
const server = require('./lib/server');

const exitWithError = (err) => {
  console.error(err);
  process.exit(1);
};

process.on('uncaughtException', exitWithError);
process.on('unhandledRejection', exitWithError);

server.start(port);
